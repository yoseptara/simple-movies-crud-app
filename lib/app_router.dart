import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:movies_collection_app/features/movies/pages/add-or-update-movie-page/add_or_update_movie_page.dart';
import 'package:movies_collection_app/features/movies/pages/movie-list-page/movie_list_page.dart';

part 'app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Page|Screen|Modal,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: MovieListRoute.page, path: MovieListPage.route),
        AutoRoute(
          page: AddOrUpdateMovieRoute.page,
          path: AddOrUpdateMoviePage.route,
        ),
      ];
}
