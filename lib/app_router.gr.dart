// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    AddOrUpdateMovieRoute.name: (routeData) {
      final args = routeData.argsAs<AddOrUpdateMovieRouteArgs>(
          orElse: () => const AddOrUpdateMovieRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: AddOrUpdateMoviePage(
          key: args.key,
          movieId: args.movieId,
        ),
      );
    },
    MovieListRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MovieListPage(),
      );
    },
  };
}

/// generated route for
/// [AddOrUpdateMoviePage]
class AddOrUpdateMovieRoute extends PageRouteInfo<AddOrUpdateMovieRouteArgs> {
  AddOrUpdateMovieRoute({
    Key? key,
    int? movieId,
    List<PageRouteInfo>? children,
  }) : super(
          AddOrUpdateMovieRoute.name,
          args: AddOrUpdateMovieRouteArgs(
            key: key,
            movieId: movieId,
          ),
          initialChildren: children,
        );

  static const String name = 'AddOrUpdateMovieRoute';

  static const PageInfo<AddOrUpdateMovieRouteArgs> page =
      PageInfo<AddOrUpdateMovieRouteArgs>(name);
}

class AddOrUpdateMovieRouteArgs {
  const AddOrUpdateMovieRouteArgs({
    this.key,
    this.movieId,
  });

  final Key? key;

  final int? movieId;

  @override
  String toString() {
    return 'AddOrUpdateMovieRouteArgs{key: $key, movieId: $movieId}';
  }
}

/// generated route for
/// [MovieListPage]
class MovieListRoute extends PageRouteInfo<void> {
  const MovieListRoute({List<PageRouteInfo>? children})
      : super(
          MovieListRoute.name,
          initialChildren: children,
        );

  static const String name = 'MovieListRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
