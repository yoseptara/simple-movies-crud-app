import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';
import 'package:movies_collection_app/features/movies/models/movie_model.dart';

part 'movie_list_store.g.dart';

class MovieListStore = MovieListBase with _$MovieListStore;

abstract class MovieListBase with Store {
  int newId = 0;

  ObservableMap<int, MovieModel> movies = ObservableMap<int, MovieModel>();

  @computed
  ObservableMap<int, MovieModel> get filteredMovies {
    if (query.isEmpty) {
      return movies;
    }

    return ObservableMap.of(movies)
      ..removeWhere((key, value) {
        final title = value.title.trim().toLowerCase();
        final director = value.director.trim().toLowerCase();

        return title.contains(query) == false && director.contains(query) == false;
      });
  }

  @observable
  String query = '';

  @action
  void updateQuery(String newQuery) {
    query = newQuery.trim().toLowerCase();
  }


  @action
  void addMovie({
    required String title,
    required String director,
    required String summary,
    required List<Genre> genres,
  }) {
    movies[newId] = MovieModel(
      id: newId,
      title: title,
      director: director,
      summary: summary,
      genres: genres,
    );
    newId++;
  }

  @action
  void updateMovie(MovieModel movie) {
    movies[movie.id] = movie;
  }

  @action
  void deleteMovie(int id) {
    movies.remove(id);
  }
}
