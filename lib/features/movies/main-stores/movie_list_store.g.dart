// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_list_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieListStore on MovieListBase, Store {
  Computed<ObservableMap<int, MovieModel>>? _$filteredMoviesComputed;

  @override
  ObservableMap<int, MovieModel> get filteredMovies =>
      (_$filteredMoviesComputed ??= Computed<ObservableMap<int, MovieModel>>(
              () => super.filteredMovies,
              name: 'MovieListBase.filteredMovies'))
          .value;

  late final _$queryAtom = Atom(name: 'MovieListBase.query', context: context);

  @override
  String get query {
    _$queryAtom.reportRead();
    return super.query;
  }

  @override
  set query(String value) {
    _$queryAtom.reportWrite(value, super.query, () {
      super.query = value;
    });
  }

  late final _$MovieListBaseActionController =
      ActionController(name: 'MovieListBase', context: context);

  @override
  void updateQuery(String newQuery) {
    final _$actionInfo = _$MovieListBaseActionController.startAction(
        name: 'MovieListBase.updateQuery');
    try {
      return super.updateQuery(newQuery);
    } finally {
      _$MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addMovie(
      {required String title,
      required String director,
      required String summary,
      required List<Genre> genres}) {
    final _$actionInfo = _$MovieListBaseActionController.startAction(
        name: 'MovieListBase.addMovie');
    try {
      return super.addMovie(
          title: title, director: director, summary: summary, genres: genres);
    } finally {
      _$MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateMovie(MovieModel movie) {
    final _$actionInfo = _$MovieListBaseActionController.startAction(
        name: 'MovieListBase.updateMovie');
    try {
      return super.updateMovie(movie);
    } finally {
      _$MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteMovie(int id) {
    final _$actionInfo = _$MovieListBaseActionController.startAction(
        name: 'MovieListBase.deleteMovie');
    try {
      return super.deleteMovie(id);
    } finally {
      _$MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
query: ${query},
filteredMovies: ${filteredMovies}
    ''';
  }
}
