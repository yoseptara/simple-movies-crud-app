enum Genre {
  action,
  animation,
  drama,
  scifi, horror;

  String get label {
    switch (this) {
      case Genre.action:
        return 'Action';
      case Genre.animation:
        return 'Animation';
      case Genre.drama:
        return 'Drama';
      case Genre.scifi:
        return 'Sci-Fi';
      case Genre.horror:
        return 'Horror';
    }
  }

  @override
  String toString() {
    return label;
  }
}

class MovieModel {
  final int id;
  final String title;
  final String director;
  final String summary;
  final List<Genre> genres;

  MovieModel({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });

  @override
  String toString() {
    return 'MovieModel{id: $id, title: $title, director: $director, summary: $summary, genres: $genres}';
  }
}
