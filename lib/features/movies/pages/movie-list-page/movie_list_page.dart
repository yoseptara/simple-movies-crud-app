import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:movies_collection_app/app_router.dart';
import 'package:movies_collection_app/constants.dart';
import 'package:movies_collection_app/features/movies/main-stores/movie_list_store.dart';

@RoutePage()
class MovieListPage extends StatelessWidget {
  static const route = '/';

  const MovieListPage({super.key});

  @override
  Widget build(BuildContext context) {
    final movieStore = GetIt.I.get<MovieListStore>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies Collection'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.router.push(AddOrUpdateMovieRoute());
        },
        child: const Icon(Icons.add),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            TextField(
              onChanged: (value) {
                movieStore.updateQuery(value);
              },
              decoration: InputDecoration(
                  hintText: 'Search movie name or director name here...',
                  hintStyle: TextStyle(
                      fontSize: 12, color: Colors.grey.shade500
                  ),
                  border: kDefaultInputBorder,
                  focusedBorder: kDefaultInputBorder,
                  enabledBorder: kDefaultInputBorder),
            ),
            const SizedBox(height: 24,),
            Expanded(
              child: Observer (
                  builder: (context) {
                    if (movieStore.filteredMovies.isEmpty) {
                      return const Center(
                        child: Text(
                          'Movies is still empty, add movie first',
                        ),
                      );
                    }

                    return ListView.builder(
                      itemBuilder: (context, index) {
                        final entry = movieStore.filteredMovies.entries.elementAt(index);
                        final movie = entry.value;

                        return Column(
                          children: [
                            const SizedBox(
                              height: 12,
                            ),
                            InkWell(
                              onTap: () {
                                context.router.push(
                                  AddOrUpdateMovieRoute(
                                    movieId: movie.id,
                                  ),
                                );
                              },
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                  border: Border.all(
                                    color: Colors.black,
                                    width: 1,
                                  ),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: [
                                    Text(
                                      movie.title,
                                      textAlign: TextAlign.start,
                                    ),
                                    Text(
                                      movie.director,
                                      textAlign: TextAlign.start,
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      movie.genres.join(' / '),
                                      textAlign: TextAlign.right,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                      itemCount: movieStore.filteredMovies.length,
                    );
                  }
              ),
            ),
          ],
        ),
      ),
    );
  }
}
