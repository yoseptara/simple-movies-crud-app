import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movies_collection_app/constants.dart';
import 'package:movies_collection_app/features/movies/models/movie_model.dart';
import 'package:movies_collection_app/features/movies/pages/add-or-update-movie-page/add_or_update_movie_store.dart';

@RoutePage()
class AddOrUpdateMoviePage extends StatelessWidget {
  static const route = '/add_or_update_movie_page';

  final int? movieId;

  const AddOrUpdateMoviePage({super.key, this.movieId});

  @override
  Widget build(BuildContext context) {
    final addOrUpdateMovieStore = AddOrUpdateMovieStore(movieId);
    final formKey = GlobalKey<FormState>();

    return Observer(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          actions: [
            if (movieId != null)
              TextButton(
                onPressed: () {
                  addOrUpdateMovieStore.delete(context);
                },
                child: const Text(
                  'Delete',
                ),
              ),
            TextButton(
              onPressed: addOrUpdateMovieStore.isMovieUpdated ? () {
                addOrUpdateMovieStore.submit(context, formKey);
              } : null,
              child: Text(
                'Save',
                style: TextStyle(
                  color: addOrUpdateMovieStore.isMovieUpdated ? Colors.deepPurple : Colors.grey
                ),
              ),
            ),
          ],
        ),
        body: Form(
          key: formKey,
          autovalidateMode: addOrUpdateMovieStore.formAutovalidate,
          child: ListView(
            padding: const EdgeInsets.all(20),
            children: [
              TextFormField(
                initialValue: addOrUpdateMovieStore.title,
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'This field is required';
                  }

                  return null;
                },
                onChanged: (String value) {
                  addOrUpdateMovieStore.updateTitle(value);
                },
                decoration: const InputDecoration(
                  labelText: 'Title',
                  border: kDefaultInputBorder,
                  focusedBorder: kDefaultInputBorder,
                  enabledBorder: kDefaultInputBorder,
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              TextFormField(
                initialValue: addOrUpdateMovieStore.director,
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'This field is required';
                  }
                  return null;
                },
                onChanged: (String value) {
                  addOrUpdateMovieStore.updateDirector(value);
                },
                decoration: const InputDecoration(
                  labelText: 'Director',
                  border: kDefaultInputBorder,
                  focusedBorder: kDefaultInputBorder,
                  enabledBorder: kDefaultInputBorder,
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              TextFormField(
                initialValue: addOrUpdateMovieStore.summary,
                onChanged: (String value) {
                  addOrUpdateMovieStore.updateSummary(value);
                },
                maxLength: 100,
                decoration: const InputDecoration(
                  labelText: 'Summary',
                  border: kDefaultInputBorder,
                  focusedBorder: kDefaultInputBorder,
                  enabledBorder: kDefaultInputBorder,
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              Wrap(
                alignment: WrapAlignment.start,
                spacing: 5,
                children: List<Widget>.generate(
                  Genre.values.length,
                  (index) {
                    final genre = Genre.values[index];
                    return InputChip(
                      // selectedColor: Colors.black,
                      label: Text(
                        genre.label,
                      ),
                      selected: addOrUpdateMovieStore.genres.contains(genre),
                      onSelected: (bool selected) {
                        if (selected) {
                          addOrUpdateMovieStore.addGenre(genre);
                        } else {
                          addOrUpdateMovieStore.removeGenre(genre);
                        }
                      },
                    );
                  },
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
