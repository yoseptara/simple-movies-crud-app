// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_or_update_movie_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$AddOrUpdateMovieStore on AddOrUpdateMovieBase, Store {
  Computed<bool>? _$isMovieUpdatedComputed;

  @override
  bool get isMovieUpdated =>
      (_$isMovieUpdatedComputed ??= Computed<bool>(() => super.isMovieUpdated,
              name: 'AddOrUpdateMovieBase.isMovieUpdated'))
          .value;

  late final _$isTitleUpdatedAtom =
      Atom(name: 'AddOrUpdateMovieBase.isTitleUpdated', context: context);

  @override
  bool get isTitleUpdated {
    _$isTitleUpdatedAtom.reportRead();
    return super.isTitleUpdated;
  }

  @override
  set isTitleUpdated(bool value) {
    _$isTitleUpdatedAtom.reportWrite(value, super.isTitleUpdated, () {
      super.isTitleUpdated = value;
    });
  }

  late final _$isDirectorUpdatedAtom =
      Atom(name: 'AddOrUpdateMovieBase.isDirectorUpdated', context: context);

  @override
  bool get isDirectorUpdated {
    _$isDirectorUpdatedAtom.reportRead();
    return super.isDirectorUpdated;
  }

  @override
  set isDirectorUpdated(bool value) {
    _$isDirectorUpdatedAtom.reportWrite(value, super.isDirectorUpdated, () {
      super.isDirectorUpdated = value;
    });
  }

  late final _$isSummaryUpdatedAtom =
      Atom(name: 'AddOrUpdateMovieBase.isSummaryUpdated', context: context);

  @override
  bool get isSummaryUpdated {
    _$isSummaryUpdatedAtom.reportRead();
    return super.isSummaryUpdated;
  }

  @override
  set isSummaryUpdated(bool value) {
    _$isSummaryUpdatedAtom.reportWrite(value, super.isSummaryUpdated, () {
      super.isSummaryUpdated = value;
    });
  }

  late final _$isGenresUpdatedAtom =
      Atom(name: 'AddOrUpdateMovieBase.isGenresUpdated', context: context);

  @override
  bool get isGenresUpdated {
    _$isGenresUpdatedAtom.reportRead();
    return super.isGenresUpdated;
  }

  @override
  set isGenresUpdated(bool value) {
    _$isGenresUpdatedAtom.reportWrite(value, super.isGenresUpdated, () {
      super.isGenresUpdated = value;
    });
  }

  late final _$formAutovalidateAtom =
      Atom(name: 'AddOrUpdateMovieBase.formAutovalidate', context: context);

  @override
  AutovalidateMode get formAutovalidate {
    _$formAutovalidateAtom.reportRead();
    return super.formAutovalidate;
  }

  @override
  set formAutovalidate(AutovalidateMode value) {
    _$formAutovalidateAtom.reportWrite(value, super.formAutovalidate, () {
      super.formAutovalidate = value;
    });
  }

  late final _$AddOrUpdateMovieBaseActionController =
      ActionController(name: 'AddOrUpdateMovieBase', context: context);

  @override
  void updateTitle(String newTitle) {
    final _$actionInfo = _$AddOrUpdateMovieBaseActionController.startAction(
        name: 'AddOrUpdateMovieBase.updateTitle');
    try {
      return super.updateTitle(newTitle);
    } finally {
      _$AddOrUpdateMovieBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateDirector(String newDirector) {
    final _$actionInfo = _$AddOrUpdateMovieBaseActionController.startAction(
        name: 'AddOrUpdateMovieBase.updateDirector');
    try {
      return super.updateDirector(newDirector);
    } finally {
      _$AddOrUpdateMovieBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateSummary(String newSummary) {
    final _$actionInfo = _$AddOrUpdateMovieBaseActionController.startAction(
        name: 'AddOrUpdateMovieBase.updateSummary');
    try {
      return super.updateSummary(newSummary);
    } finally {
      _$AddOrUpdateMovieBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addGenre(Genre genre) {
    final _$actionInfo = _$AddOrUpdateMovieBaseActionController.startAction(
        name: 'AddOrUpdateMovieBase.addGenre');
    try {
      return super.addGenre(genre);
    } finally {
      _$AddOrUpdateMovieBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeGenre(Genre genre) {
    final _$actionInfo = _$AddOrUpdateMovieBaseActionController.startAction(
        name: 'AddOrUpdateMovieBase.removeGenre');
    try {
      return super.removeGenre(genre);
    } finally {
      _$AddOrUpdateMovieBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void submit(BuildContext context, GlobalKey<FormState> formKey) {
    final _$actionInfo = _$AddOrUpdateMovieBaseActionController.startAction(
        name: 'AddOrUpdateMovieBase.submit');
    try {
      return super.submit(context, formKey);
    } finally {
      _$AddOrUpdateMovieBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void delete(BuildContext context) {
    final _$actionInfo = _$AddOrUpdateMovieBaseActionController.startAction(
        name: 'AddOrUpdateMovieBase.delete');
    try {
      return super.delete(context);
    } finally {
      _$AddOrUpdateMovieBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isTitleUpdated: ${isTitleUpdated},
isDirectorUpdated: ${isDirectorUpdated},
isSummaryUpdated: ${isSummaryUpdated},
isGenresUpdated: ${isGenresUpdated},
formAutovalidate: ${formAutovalidate},
isMovieUpdated: ${isMovieUpdated}
    ''';
  }
}
