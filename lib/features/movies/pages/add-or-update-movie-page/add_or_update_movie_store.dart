import 'package:flutter/foundation.dart'; // For listEquals
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:movies_collection_app/features/movies/main-stores/movie_list_store.dart';
import 'package:movies_collection_app/features/movies/models/movie_model.dart';

part 'add_or_update_movie_store.g.dart';

class AddOrUpdateMovieStore = AddOrUpdateMovieBase with _$AddOrUpdateMovieStore;

abstract class AddOrUpdateMovieBase with Store {
  final int? movieId;

  AddOrUpdateMovieBase(this.movieId);

  final _movieListStore = GetIt.I.get<MovieListStore>();

  late final _movie = _movieListStore.movies[movieId];
  late String title = _movie?.title ?? '';
  late String director = _movie?.director ?? '';
  late String summary = _movie?.summary ?? '';
  late ObservableList<Genre> genres = ObservableList.of(_movie?.genres ?? []);

  @observable
  bool isTitleUpdated = false;

  @observable
  bool isDirectorUpdated = false;

  @observable
  bool isSummaryUpdated = false;

  @observable
  bool isGenresUpdated = false;

  @computed
  bool get isMovieUpdated =>
      isTitleUpdated || isDirectorUpdated || isSummaryUpdated || isGenresUpdated;

  @observable
  AutovalidateMode formAutovalidate = AutovalidateMode.disabled;

  @action
  void updateTitle(String newTitle) {
    title = newTitle;

    if (_movie?.title != title) {
      isTitleUpdated = true;
    } else {
      isTitleUpdated = false;
    }
  }

  @action
  void updateDirector(String newDirector) {
    director = newDirector;

    if (_movie?.director != director) {
      isDirectorUpdated = true;
    } else {
      isDirectorUpdated = false;
    }
  }

  @action
  void updateSummary(String newSummary) {
    summary = newSummary;

    if (_movie?.summary != summary) {
      isSummaryUpdated = true;
    } else {
      isSummaryUpdated = false;
    }
  }

  @action
  void addGenre(Genre genre) {
    genres.add(genre);

    if (listEquals(_movie?.genres, genres)) {
      isGenresUpdated = false;
    } else {
      isGenresUpdated = true;
    }
  }

  @action
  void removeGenre(Genre genre) {
    genres.remove(genre);

    if (listEquals(_movie?.genres, genres)) {
      isGenresUpdated = false;
    } else {
      isGenresUpdated = true;
    }
  }

  @action
  void submit(BuildContext context, GlobalKey<FormState> formKey) {
    if (formKey.currentState?.validate() == true) {
      if (movieId == null) {
        _movieListStore.addMovie(
          title: title,
          director: director,
          summary: summary,
          genres: genres,
        );
      } else {
        _movieListStore.updateMovie(
          MovieModel(
            id: movieId!,
            title: title,
            director: director,
            summary: summary,
            genres: genres,
          ),
        );
      }
      context.router.pop();
    } else {
      formAutovalidate = AutovalidateMode.always;
    }
  }

  @action
  void delete(BuildContext context) {
    _movieListStore.deleteMovie(movieId!);
    context.router.pop();
  }
}
