import 'package:flutter/material.dart';

const kDefaultInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(12),
    ),
    borderSide: BorderSide(width: 1, color: Colors.black)
);